package com.example.springboot_activiti_demo.activiti.util;

import com.example.springboot_activiti_demo.activiti.Entity.response.RespEntity;
import net.sf.json.JSONArray;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author: Mingfang Zhu
 * @Description:  json工具类
 * @Date: Created in 15:16 2018/5/15
 */
public class JsonUtils {

    public static void jsonpFormat(HttpServletRequest request, HttpServletResponse response, RespEntity respEntity) throws IOException {

        //设置页面中为中文编码展示，同时调用了setCharacterEncoding方法设置了设置response容器编码
        response.setContentType("text/html;charset=UTF-8");
        //数据通过jsonp形式发送到前端
        JSONArray jsonArray = JSONArray.fromObject(respEntity);
        String result = jsonArray.toString();
        String callback = request.getParameter("callback");
        result = callback + "(" + result + ")";
        response.getWriter().write(result);
    }

}
