package com.example.springboot_activiti_demo.activiti.service;

import com.example.springboot_activiti_demo.activiti.Entity.response.RespEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * @Author: Mingfang Zhu
 * @Description: 流程服务层接口
 * @Date: Created in 13:43 2018/5/15
 */
@Service
public interface WorkFlowService {


    /**
     * 审批任务（下一节点为单实例userTask）
     * @param processKey
     * @param businessKey
     * @param variables
     * @return
     */
    public RespEntity approveTask(String processKey,String businessKey,Map variables,String assignee);


    /**
     * 根据流程定义key和用户id查询待办任务
     * @param processKey
     * @param assignee
     * @return
     */
    public RespEntity queryToDoTask(String processKey,String assignee);

    /**
     * 启动流程接口
     * @param businessKey  业务主键
     * @param assigneeVars  申请人参数（以map形式传入）
     * @param nextAssigneeVars  下一审批人参数
     * @param processKey      流程定义key
     * @return
     */
    public RespEntity startWorkFlow(String businessKey, Map assigneeVars,Map nextAssigneeVars, String processKey);

    /**
     * 终止流程
     * @param processKey
     * @param businessKey
     * @return
     */
    public RespEntity terminateWorkFlow(String processKey,String businessKey);


    /**
     * 查询历史审批列表
     * @param businessKey
     * @param assigneeId
     * @return
     */
    public RespEntity queryHistoryTaskList(String businessKey,String assigneeId);


    /**
     * 撤回任务
     * @param processKey
     * @param businessKey
     * @return
     */
    public RespEntity withdraw(String processKey,String businessKey);


    /**
     * 追踪流程图
     * @param response
     * @param processKey
     * @param businessKey
     * @return
     * @throws IOException
     */
    public RespEntity traceFlowDiagram(HttpServletResponse response,String processKey, String businessKey) throws IOException;


    /**
     * 发布流程
     * @param file
     * @return
     */
    public RespEntity releaseProcess(MultipartFile file);
}
