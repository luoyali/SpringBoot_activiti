package com.example.springboot_activiti_demo.activiti.controller;

import com.example.springboot_activiti_demo.activiti.Entity.response.RespEntity;
import com.example.springboot_activiti_demo.activiti.service.WorkFlowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;



/**
 * @Author: Mingfang Zhu
 * @Description:
 * @Date: Created in 19:16 2018/7/20
 */
@Controller
@RequestMapping("/web")
public class WebController {

    @Autowired
    private WorkFlowService workFlowService;

    @RequestMapping("/uploadProcess")
    public String releaseProcess(){
        return "uploadProcess";
    }

    @RequestMapping("/releaseProcess")
    @ResponseBody
    public RespEntity upload(MultipartFile file){
        RespEntity respEntity = workFlowService.releaseProcess(file);
        return respEntity;
    }



}
