<%--
  Created by IntelliJ IDEA.
  User: Pengzili
  Date: 2018/2/9
  Time: 10:35
  To change this template use File | Settings | File Templates.
--%>
<%--<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>--%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>请假申请</title>
    <script type="text/javascript" language="javascript" src="js/common/jquery-1.8.3.js"></script>
</head>
<body>


<c:if test="${not empty error}">
    <div id="error" class="alert alert-error">${error}</div>
</c:if>
<c:if test="${not empty message}">
    <div id="message" class="alert alert-success">${message}</div>
</c:if>
<form action="task/leave/start" method="post">
    <fieldset>
        <legend>请假申请</legend>
        <p>
            <label>单号：</label>
            <input name="reportNo" required="reportNo">
        <p>
        <p>
            <label>员工：</label>
            <input name="userId" required="required">
        <p>
        <p>
            <label>审批人：</label>
            <input name="managerId" required="required">
        <p>
        <p>
            <label>请假类型</label>
            <select id="leaveType" name="leaveType" required="required">
                <option>公休</option>
                <option>病假</option>
                <option>调休</option>
                <option>事假</option>
                <option>婚假</option>
            </select>
        </p>
        <p>
        <label>请假天数</label>
        <input  name="day" required="required">
        </p>
        <p>
            <label>开始时间</label>
            <input type="date" name="startTime" required="required">
        </p>
        <p>
            <label>结束时间</label>
            <input type="date" name="endTime" required="required">
        </p>
        <p>
            <label>请假原因</label>
            <textarea id="reason" name="reason"></textarea>
        </p>
        <p>
            <input type="submit" value="启动流程">
        </p>

    </fieldset>
</form>
<input id="approveName" placeholder="请输入申请人名字">
<input id="toApproveList" type="button" value="到审批人列表" >


<script type="text/javascript">
    $(function () {

    });
    $("#toApproveList").on("click",function(){
        var approveName = $("#approveName").val();
        alert(approveName);
       window.location.href = "task/list?userId="+approveName;
    });
</script>

</body>
</html>
