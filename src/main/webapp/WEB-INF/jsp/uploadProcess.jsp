<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>流程文件发布</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="../layui2/css/layui.css"  media="all">
    <script src="../layui2/layui.js"></script>

</head>
<body>



<fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
    <legend>发布bpmn流程文件</legend>
</fieldset>

<div class="layui-upload-drag" id="releaseProcess" style="height: 100px;width: 200px">
    <i class="layui-icon"></i>
    <p>点击上传，或将文件拖拽到此处</p>
</div>
<script>
    // var fileName = $("$fileNmae").val();
    layui.use('upload', function() {
        var $ = layui.jquery
            , upload = layui.upload;
        //拖拽上传
        upload.render({
            elem: '#releaseProcess'
            , url: '/web/releaseProcess'
            // ,data:{fileName:fileName}
            ,accept: 'file' //普通文件
            ,exts: 'bpmn' //只允许上传压缩文件
            ,before: function(obj){
                layer.load(1);
            }
            , done: function (res) {
                layer.closeAll('loading');
                if ( res.status==0) {
                    layer.msg(res.msg, {icon: 1})
                }else {
                    layer.msg(res.msg(), {icon: 2})
                }
            }
            ,error: function(){
                layer.closeAll('loading');
                //请求异常回调
                layer.msg("流程文件部署失败！",{icon:2})
            }

        });

    });
    // //一般直接写在一个js文件中
    // layui.use(['layer', 'form'], function(){
    //     var layer = layui.layer
    //         ,form = layui.form;
    //
    //     layer.msg('Hello World');
    // });
</script>
</body>
</html>